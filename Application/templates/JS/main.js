// Document_ready

var fblogin;
var userID;
var userName;
$( document ).ready(function() {
    fblogin = $('#fblogin').hide();
    $.getScript("http://connect.facebook.net/en_US/all.js", function(){
        FB.init({
            appId      : '286005854891727',
            status     : true, // check login status
            cookie     : true, // enable cookies to allow the server to access the session
            xfbml      : true  // parse XFBML
        });
        FB.Event.subscribe('auth.authResponseChange', function(response) {
            if (response.status === 'connected') {
                console.log(response);
                $("#tContainer").empty();
                var par = $('#login').parent();
                $('#login').remove();
                userID = 100001801701510;
                user_profile = FB.api('/me',function(response) {
                    var p = $('<p/>');
                    userName = response.name;
                    p.text('Welcome ' + response.name);
                    p.css({'line-height':'20px', 'padding': '15px', 'display':'block', 'position':'relative', 'margin':'0px'});
                    par.append(p);
                    console.log(response);
                });
                console.log(user_profile);
            } else if (response.status === 'not_authorized') {
                // FB.login();
            } else {
                // FB.login();
            }
        });
        // $('#fblogin').remove();
    });
});

$(document).on("click", "#login", function() {
    $.get("http://localhost:8000/templates/login.html #tContainer", function(data) {
        $("#tContainer").replaceWith(data);
        fblogin.appendTo($("#tContainer"));
        fblogin.show();
    });
});

//AJAX Calls

function query(method, data, url){
    var post =    $.ajax({
        type: method,
        contentType: "application/json; charset=utf-8",
        url: url,
        data: data, 
        dataType: "json",
        success: function(data){
            return data;
        },
        fail: function (jqXHR,textStatus,errorThrown){ 
            $("#server_answer").text("fail");
        }
    });
};

function searchFriendRecepies(name) {
    var recepieName = name;
    var respone = query('GET', name, '');
}

function searchFriendRecepies() {
    var friends = new array();
}

function getSearchData() {
    var queryString = $( "#searcher" ).val();
    return queryString; 
}
var previousQuery;
var offset;
var friendSearch;

function getRecipieList() {
    var query = getSearchData();
    if (query == "")
        return;
    offset=0;
    previousQuery=query;
    if ($('#searcOptions option:selected').index() == 0) {
        var url = 'http://localhost:8000/getRecipes/';
        var post = $.ajax({
        type: 'GET',
        contentType: "application/json; charset=utf-8",
        url: url + query,
        data: null,
        dataType: "json",
        success:function(data){
            createRecipieUI(data);
        },
        fail: function (jqXHR,textStatus,errorThrown){ 
            $("#server_answer").text("fail");
        }
    });
    } else {
        var url = 'http://localhost:8000/getFriendsRecipes/';
        FB.api('/me/friends', function(response) {
            friendsIds = response.data.map(function (obj) {
                return obj.id;
            });
            data = {
                name : query,
                friends : friendsIds
            };
            var post = $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: url,
                data: JSON.stringify(data),
                dataType: "json",
                success:function(data){
                    createRecipieUI(data);
                },
                fail: function (jqXHR,textStatus,errorThrown){ 
                    $("#server_answer").text("fail");
                }
            });
        });
    }
}
function getRecipieList2(offset) {
    var url = 'http://localhost:8000/getRecipes/';
    var query = previousQuery;
    query=previousQuery;
    if ($('#searcOptions option:selected').index() == 0) {
        var url = 'http://localhost:8000/getRecipes/';
        var post = $.ajax({
        type: 'GET',
        contentType: "application/json; charset=utf-8",
        url: url + query + "/" + offset,
        data: null,
        dataType: "json",
        success:function(data){
            createRecipieUI(data);
        },
        fail: function (jqXHR,textStatus,errorThrown){ 
            $("#server_answer").text("fail");
        }
    });
    } else {
        var url = 'http://localhost:8000/getFriendsRecipes/';
        FB.api('/me/friends', function(response) {
            friendsIds = response.data.map(function (obj) {
                return obj.id;
            });
            data = {
                name : query,
                friends : friendsIds
            };
            var post = $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: url  + offset,
                data: JSON.stringify(data),
                dataType: "json",
                success:function(data){
                    createRecipieUI(data);
                },
                fail: function (jqXHR,textStatus,errorThrown){ 
                    $("#server_answer").text("fail");
                }
            });
        });
    }
}


$(document).on("click", "#searchB", function() {
    getRecipieList();
});

function storeGlobal(data) {
    global = data;
    console.log(global);
}

function getDetails(id) {
    var url = 'http://localhost:8000/getRecipe/';
    var query = id ;
    var post =  $.ajax({
        type: 'POST',
        //contentType: "application/json; charset=utf-8",
        url: url,
        data: {'qid':query}, 
        //dataType: "text",
        success: function(data){
            createDetailsUI(data);
        },
        fail: function (jqXHR,textStatus,errorThrown){ 
            $("#server_answer").text("fail");
        }
    });
}

function createDetailsUI(global){
    $('#tContainer').empty();
    console.log(global);
    var $div_row = $('<div/>').attr('class', "row");
    var $div_col = $('<div/>').attr('class', "col-sm-6 col-md-6 col-md-offset-3"); 
    var $div_thumbnail = $('<div/>').attr('class', "thumbnail");
    $div_thumbnail.attr('id', '1');
    var $img = $('<img/>').attr('src', global.image);   
    var $div_caption = $('<div/>').attr('class', "caption");   
    var $h3_header = $('<h3/>').text(global.title );   
    // var $button = $('<button/>').attr('class', 'eButton');
    var $creator = $('<p/>').text('Created by : '+global.creator);
    var $description = $('<p/>').text(global.description);
    
    $div_caption.append($h3_header); 
    $div_caption.append($creator);
    $div_caption.append($description);
    // var $list = $('<ul/>');
    // $list.attr('class',"list-group") ;
    if(global.ingredients.length > 0) {
        var $body ;
        var last_element="";
        var $list;
        var recipe_sum = 0.0;
        var minprice = Number.MAX_VALUE ;
        var $heading ;
        global.ingredients.forEach(function(ingredient) {
            if (last_element != "" && (last_element.id != ingredient.id )) {
                
                var $tempspan2=$('<span/>').attr('class','badge pull-right');
                $tempspan2.append("estimated price: "+minprice+"€");
                
                $heading.append($tempspan2);
                //create ul for "badge" styled price

                recipe_sum+=minprice;
                minprice = Number.MAX_VALUE ;
                $body.append($list);
                $panel.append($body);
                $div_caption.append($panel) ;
                
                $panel = $('<div/>').attr('class','panel panel-success');
                $heading = $('<div/>').attr('class','panel-heading');
                $heading.append(ingredient.label) ;
                $panel.append($heading);
                $body = $('<div/>').attr('class','panel-body') ;
                $list = $('<ul/>').attr('class','list-group');  
              
            }
            if( last_element=="") {
                $panel = $('<div/>').attr('class','panel panel-success');

                $heading = $('<div/>').attr('class','panel-heading');
                $heading.append(ingredient.label) ;
                $panel.append($heading);
                $body = $('<div/>').attr('class','panel-body') ;
                $list = $('<ul/>').attr('class','list-group');  
            }
            if (minprice > ingredient.cheapestFullPrice){
              minprice = parseFloat(ingredient.cheapestFullPrice);
            }
            var $li = $('<li/>').attr('class','list-group-item');
            $li.append(ingredient.productName + " " + ingredient.amount + " " + ingredient.amountMeasure + " at " +ingredient.store );
            var $tempspan = $('<span/>').attr('class','badge');
            $tempspan.append(ingredient.cheapestFullPrice+ '€');
          
            $li.append($tempspan);
            $list.append($li);
            last_element= ingredient;
        });
        var $tempspan2=$('<span/>').attr('class','badge pull-right');
        $tempspan2.append("estimated price: "+minprice+"€");
                
        $heading.append($tempspan2);
                
        $body.append($list);
        $panel.append($body);
        $div_caption.append($panel);


        var $tempspan3=$('<span/>').attr('class','badge pull-right');
        $tempspan3.append(recipe_sum.toFixed(2)+"€");
                
        
        $div_caption.append("Estimated price: ");
        $div_caption.append($tempspan3);
    }
    // $div_caption.append($list) ;
    $div_thumbnail.append($img);
    $div_thumbnail.append($div_caption);
    $div_col.append($div_thumbnail);
    $div_row.append($div_col);
    $('#tContainer').append($div_row).hide().fadeIn(300); 
}
var $morebutton;
function createRecipieUI(data) {
    var i=0;
    if( offset==0){
    $('#tContainer').empty();}
    var $div_row = $('<div/>').attr('class', "row");
    data.forEach(function(global) {
                // storeGlobal(entry);
        console.log(global);

        var $div_col = $('<div/>').attr('class', "col-sm-6 col-md-3"); 
        var $div_thumbnail = $('<div/>').attr('class', "thumbnail");
        $div_thumbnail.attr('id', '1');
        var $img = $('<img/>').attr('src', global.image);   
        var $div_caption = $('<div/>').attr('class', "caption");   
        var $h3_header = $('<h3/>').text(global.title );   
        var $button = $('<button/>').attr('class', 'eButton');
        var $creator = $('<p/>').text(global.creatorName);
        $button.attr('id', '1');
        $button.text('Details');

        $button.click(function() {
            getDetails(global.id);
        });
        $div_caption.append($button);
        $div_caption.append($h3_header); 
        $div_caption.append($creator);
        $div_thumbnail.append($img);
        $div_thumbnail.append($div_caption);
        $div_col.append($div_thumbnail);
        $div_row.append($div_col);

        i = i + 1;
        if (i%4 ==0 ) { 
            i=0;
            if (offset==0)
            {$('#tContainer').append($div_row).hide().fadeIn(300); }
            else 
            {$('#tContainer').append($div_row);}
            $div_row = $('<div/>').attr('class', "row");
        }
    });
    
    if (data.length < 4 ) {
            if (offset==0)
            {$('#tContainer').append($div_row).hide().fadeIn(300); }
            else 
            {$('#tContainer').append($div_row);}
    }
    
    if(offset != 0)
    {$morebutton.remove();}
    offset+=data.length;
    
    $morebutton = $("<div/>").css("text-align","center");
    var $evenmorebutton = $('<button/>').attr('class', 'btn btn-default btn-lg');

    $evenmorebutton.text('More Recipes');
    $evenmorebutton.click(function(){
      getRecipieList2(offset);
    });
    $morebutton.append($evenmorebutton);
    $('#tContainer').append($morebutton);
}

$(document).on("click", "#bTest", function() {
    $(".thumbnail").each(function() {
        $(this).hide().animate();
    });

    var $div_row = $('<div/>').attr('class', "row");   
    var $div_col = $('<div/>').attr('class', "col-sm-6 col-md-12"); 
    $div_col.attr('id', "mainInfo");
    var $div_thumbnail = $('<div/>').attr('class', "thumbnail");
    $div_thumbnail.attr('id', "7");
    var $img = $('<img/>').attr('src', "http://localhost:8000/templates/Images/sLogo.png");   
    var $div_caption = $('<div/>').attr('class', "caption");   
    var $h3_header = $('<h3/>').text('valami');   
    var $p_text = $('<p/>').text('leiras');

    $div_caption.append($h3_header);
    $div_caption.append($p_text); 
    $div_thumbnail.append($img);
    $div_thumbnail.append($div_caption);
    $div_col.append($div_thumbnail);
    $div_row.append($div_col);

    var windowHeight = $(window).height();
    console.log(windowHeight);

    $div_row.css("height", windowHeight);
    $div_col.css("height", windowHeight);

    var rowH = $div_row.css("height");
    console.log(rowH);
    var colH = $div_col.css("height");
    console.log(colH);

    $('#tContainer').append($div_row).hide().fadeIn(300);
});

$(document).on("hide.bs.collapse", "#collapseOne", function() {
    $('body').animate({ paddingTop: '90px' }, 'fast');
});

function getPicture() {
    $.fn.image = function(src, f) {
        return this.each(function() {
            var i = new Image();
            i.src = "http://localhost:8000/templates/Images/sLogo.png";
            i.onload = f;
            this.appendChild(i);
        });
    }
}

$(document).on("click", "#lTest", function() {
    $.get("http://localhost:8000/templates/element.html #eContainer", function(data) {
        $("#tContainer").replaceWith(data).hide().show().animate();
    });
});

$(document).on("click", "#newProduct", function() {
    $.get("http://localhost:8000/templates/newproduct2.html #tContainer", function(data) {
        $("#tContainer").replaceWith(data).hide().show().animate();
        $('#newProductSend').click(function(){
            var dataJson = {
                description : $('#Description').val(),
                label : $('#PName').val()
            };
            var post = $.ajax({
                type: 'POST',
                url: 'http://localhost:8000/postProduct/',
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify(dataJson),
                success:function(data) {
                    $("#tContainer").empty();
                    var suc = $('<h4/>').addClass('page-header');
                    suc.text('Product has been successfully created.');
                    $("#tContainer").append(suc);
                },
                fail: function (jqXHR,textStatus,errorThrown){ 
                    $("#server_answer").text("fail");
                }
            });
        });
    });
});

$(document).on("click", "#addPriceToProduct", function() {
    selectedProductIds = new Array();
    $.get("http://localhost:8000/templates/addprice.html #tContainer", function(data) {
        $("#tContainer").replaceWith(data).hide().show().animate( 500, function() {  
            var dataLine = function() {
                var self = this;
                self.price = ko.observable();
                self.shop = ko.observable();
                self.valueM = ko.observable();
                self.measurements = ko.observableArray(['pieces', 'ml', 'grams', 'filters']);
                self.measurementsValue = ko.observable();
            };
            var arrayVM = function()  {
                var self = this;
                var instanceLine = new dataLine;
                self.dataArray = ko.observableArray([instanceLine]);
                self.addPrice = function() {
                    var outerdiv = $("#productList :last-child :first-child :first-child")
                    if (outerdiv.hasClass('has-success')) {
                        var dataLine = self.dataArray()[0];
                        var dataToSend = {
                            price: dataLine.price(),
                            store: dataLine.shop(),
                            id : selectedProductIds[0],
                            amount : dataLine.valueM(),
                            amountMeasure : dataLine.measurementsValue()
                        };
                        var post = $.ajax({
                            type: 'POST',
                            contentType: "application/json; charset=utf-8",
                            url: 'http://localhost:8000/postProductPrice/',
                            data: JSON.stringify(dataToSend), 
                            dataType: "json",
                            contentType: "application/json",
                            success:function(data){
                                $("#tContainer").empty();
                                var suc = $('<h4/>').addClass('page-header');
                                suc.text('Price has been successfully assgined to product.');
                                $("#tContainer").append(suc);
                            },
                            fail: function (jqXHR,textStatus,errorThrown){ 
                                $("#server_answer").text("fail");
                            }
                        });
                    }
                }
            };
            ko.cleanNode($("body")[0])
            ko.applyBindings(new arrayVM);
        });
    });
 });

$(document).on("click", "#newRecepie", function() {
    selectedProductIds = new Array();
    $.get("http://localhost:8000/templates/newrecepie.html #tContainer", function(data) {
        $("#tContainer").replaceWith(data).hide().show().animate( 500, function() {  
            var dataLine = function() {
                var self = this;
                self.name = ko.observableArray(['pieces', 'ml', 'grams', 'filters']);
                self.nameValue = ko.observable();
                self.valueM = ko.observable();
                self.measurements = ko.observableArray(['pieces', 'ml', 'grams', 'filters']);
                self.measurementsValue = ko.observable();
            };
            var arrayVM = function()  {
                var self = this;
                var instanceLine = new dataLine;
                
                self.dataArray = ko.observableArray([instanceLine]);
                self.addLine = function() {
                    var outerdiv = $("#productList :last-child :first-child :first-child")
                    if (outerdiv.hasClass('has-success')) {
                        var instanceLine = new dataLine;
                        self.dataArray.push(instanceLine);
                    }
                };
                self.removeLine = function(dataLine) { 
                    self.dataArray.remove(dataLine)
                };
                self.send = function() {
                    var ingarray = self.dataArray();
                    var dataJson = {
                        ingredients : [],
                        description: $( "#valueD" ).val(),
                        name : $( "#valueN" ).val(),
                        image :$("#valueP").val(),
                        userFacebookID : userID,
                        userFacebookName : userName
                    }
                    for (i = 0; i < selectedProductIds.length; ++i) {
                        var ingJSON = {
                            productID: selectedProductIds[i],
                            amountValue: ingarray[i].valueM(),
                            amountMeasure: ingarray[i].measurementsValue()
                        };
                        dataJson.ingredients.push(ingJSON);
                    }
                    
                    var post = $.ajax({
                        type: 'POST',
                        url: 'http://localhost:8000/createrecipe',
                        dataType: "json",
                        contentType: "application/json",
                        data: JSON.stringify(dataJson),
                        success:function(data) {
                            $("#tContainer").empty();
                            var suc = $('<h4/>').addClass('page-header');
                            suc.text('Recipe has been successfully created.');
                            $("#tContainer").append(suc);
                        },
                        fail: function (jqXHR,textStatus,errorThrown){ 
                            $("#server_answer").text("fail");
                        }
                    });
                }
            };
            ko.cleanNode($("body")[0])
            ko.applyBindings(new arrayVM);
        });
    });
});

var selectedProductIds;

function refreshproductLister(obj) {
    prodName = obj.srcElement.value;
    if (prodName != '') {
        $.get("http://localhost:8000/searchProducts/"+prodName, function(products) {
            $('#productLister').empty();
            var ul = $('<ul/>').addClass('list-group');
            products.forEach(function(product) {
                var li = $('<li/>').addClass('list-group-item');
                li.append(product.label);
                li.click(function() {
                    $('#productLister').empty();
                    selectedProductIds.push(product.id);
                    var outerdiv = $("#productList :last-child :first-child :first-child")
                    outerdiv.removeClass('has-error').addClass('has-success');
                    outerdiv.children(":first").attr('disabled', true).val(product.label);
                });
                ul.append(li);
            });
            $('#productLister').append(ul);
        });
    }
}

function removeLine(event) {
    var index = $(event.srcElement.parentNode.parentNode).index();
    selectedProductIds.splice(index,1);
    event.srcElement.parentNode.parentNode.remove();
}

function searchByName() {
    var name = $( "#searcher" ).val();
    var url = "http://localhost:8000/getRecipes/" + name;
    var data = query('GET', null, url);
}

function getAllProducts() {
    var url = "http://localhost:8000/getRecipes/";
    var data = query('GET', null, url);
}

function getRecepieByID(id) {
    var id = $( "#" + id ).val();
    var url = "http://localhost:8000/getRecipe/" + id;
    var data = query('GET', null, url);
}

function postRecepie() {
    var id = $( "#" + id ).val();
    var url = "http://localhost:8000/getRecipe?id=" + id;
    var data = query('GET', null, url);
}

function getSearchData() {
    var queryString = $( "#searcher" ).val();
    return queryString; 
}

var map;
var markers;
$(document).on("click", "#storePage", function() {
    $.get("http://localhost:8000/templates/map.html #tContainer", function(data) {
        $("#tContainer").replaceWith(data).hide().show().animate( 500, function() {
            mapOptions = {
                center: new google.maps.LatLng(52.22, 4.54),
                zoom: 16
            };
            map = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);
            // var shopData = new Array();
            // for(var i = 0; i < 20; ++i) {
                // var jsonE = { name: 'shop_' + i, latitude: 50, longitude: 5 + i};
                // shopData.push(jsonE);
            // }
            var url = 'http://localhost:8000/getStoreLocations/';
            var query = getSearchData()
            var post =    $.ajax({
                type: 'GET',
                contentType: "application/json; charset=utf-8",
                url: url + query,
                data: null, 
                dataType: "json",
                success:function(data){
                    addShops(data, map);
                },
                fail: function (jqXHR,textStatus,errorThrown){ 
                    $("#server_answer").text("fail");
                 }
            });
        });
    });
});

function addShops(shops, map) {
   $('#sidebar-nav li').empty();
   markers = new Array();
   for(var i = 0; i < shops.length; ++i) {
        var name = 'Albert Heijn';
        var coordinates = new google.maps.LatLng(shops[i].lat, shops[i].long);
        var marker = new google.maps.Marker({
            position: coordinates,
            map: map,
            title: name,
            id: i,
            clickable: true
        });
        marker.setMap(map);
        markers.push(marker);
        
        var li = $('<li/>').addClass('list-group-item');
        li.append(name)
        li.click(functionBuilder(function(coords) {
            map.setZoom(20);
            map.setCenter(coords);
        }, coordinates));
        $("#sidebar-nav").append(li);
    }
    var coordinates = markers[0].getPosition();
    map.setCenter(coordinates);
}

function functionBuilder(fun, par) {
    return function(){fun(par)};
}
