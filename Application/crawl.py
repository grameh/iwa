import urllib
import urllib2
import re
from mstranslator import Translator
translator= Translator('testapp_iwa','dky3x7Iz7rmihw7WZ96Qzpu752vbBVX6JYZwHRR0Yr4=')

class Product:
	image = ""
	price = -1
	name = u""
	amount = u""
	id = ""
	def __str__(self):
		return "(" + self.image + "," + str(self.price) + "," + self.name + "," + self.amount + "," + self.id + ")"

productCatPage = urllib.urlopen("http://www.ah.nl/appie/producten/aardappel-groente-fruit").read()

productCatPage

f = open("productCatPage.html", "w")
f.write(productCatPage)
f.close()

iter = re.finditer("(?P<id>wi\d+)", productCatPage)

prodListBase = "http://www.ah.nl/appie/vind/productlist?skipSitestat=true&originCode=CAT&productList="
prodListUrl = prodListBase
for m in iter:	
	prodListUrl = prodListUrl + m.group("id") + ","
prodListUrl = prodListUrl[:-1]
productsPage = urllib.urlopen(prodListUrl).read()

f = open("productsPage.html", "w")
f.write(productsPage)
f.close()

iter = re.finditer("<div class=\"canvas_card product draggable\"\s+data-appie=\"productpreview\"\s*>\s*<div class=\"image\">\s*<img src=\"(?P<imgref>[^\"]*)\" alt=\"(?P<name>[^\"]*)\">\s*(<div class=\"shield shield-beta shield-bonus shield-beta\">\s*<span class='shield-text-alpha'>\s*BONUS\s*</span>\s*</div>\s*|<div class=\"shield shield-beta shield-icon shield--beta\">\s*</div>)?\s*<p class=\"unit\">\s*(?P<amount>[^<]*)\s*</p>\s*((<p class=\"price\">\s*<ins>\s*(?P<price1>\d+\.<span>\d+)|<p class=\"price bonus\">\s*<del>\s*\d+\.\d+\s*</del>\s*<ins>\s*(?P<price2>\d+\.<span>\d+))\s*</span>\s*</ins>\s*</p>)?\s*</div>(.*?)wi(?P<id>\d+)", productsPage, re.DOTALL)

products = []

for m in iter:
	p = Product()
	p.image = m.group("imgref")
	p.name = m.group("name")
	p.amount = m.group("amount").strip()
	p.id = "wi" + m.group("id")
	if m.group("price1"):
		p.price = float(m.group("price1").replace("<span>", ""))
	elif m.group("price2"):
		p.price = float(m.group("price2").replace("<span>", ""))
	products.append(p)
	#print m.groupdict()

iter = re.finditer("productTaxonomyLevelId=(?P<taxonomyId>\d+)", productCatPage)
prodTaxonomyBase = "http://www.ah.nl/appie/vind/product?skipSitestat=true&originCode=CAT&productTaxonomyLevelId="
for m in iter:
	productsPage = urllib.urlopen(prodTaxonomyBase+m.group("taxonomyId")).read()
	iterinner = re.finditer("<div class=\"canvas_card product draggable\"\s+data-appie=\"productpreview\"\s*>\s*<div class=\"image\">\s*<img src=\"(?P<imgref>[^\"]*)\" alt=\"(?P<name>[^\"]*)\">\s*(<div class=\"shield shield-beta shield-bonus shield-beta\">\s*<span class='shield-text-alpha'>\s*BONUS\s*</span>\s*</div>\s*|<div class=\"shield shield-beta shield-icon shield--beta\">\s*</div>)?\s*<p class=\"unit\">\s*(?P<amount>[^<]*)\s*</p>\s*((<p class=\"price\">\s*<ins>\s*(?P<price1>\d+\.<span>\d+)|<p class=\"price bonus\">\s*<del>\s*\d+\.\d+\s*</del>\s*<ins>\s*(?P<price2>\d+\.<span>\d+))\s*</span>\s*</ins>\s*</p>)?\s*</div>(.*?)wi(?P<id>\d+)", productsPage, re.DOTALL)
	for product in iterinner:
		p = Product()
		p.image = product.group("imgref")
		p.name = product.group("name")
		p.amount = product.group("amount").strip()
		p.id = "wi" + product.group("id")
		if product.group("price1"):
			p.price = float(product.group("price1").replace("<span>", ""))
		elif product.group("price2"):
			p.price = float(product.group("price2").replace("<span>", ""))
		products.append(p)
id = 1
for product in products:
	
	# a=translator.translate(product.name, lang_from='nl', lang_to='en')
	# while 'TranslateApiException' in a:
	# 	a=translator.translate(product.name, lang_from='nl', lang_to='en')

	# print (product.name,translator.translate(product.name, lang_from='nl', lang_to='en'))
	print product
print len(products)

# request = urllib2.Request(r"http://translate.google.com/?hl=hu#nl/en/broodjes")
# opener = urllib2.build_opener()
# print request
# print dir(urllib2.Request(r"http://translate.google.com/?hl=hu#nl/en/broodjes"))
#print r"http://translate.google.com/?hl=hu#nl/en/broodjes"
#f = open("trans.html", "w")
#f.write(trans)
#f.close()