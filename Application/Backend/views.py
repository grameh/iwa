__author__ = 'anon'

from rest_framework import viewsets
from rest_framework.decorators import link
from SPARQLWrapper import SPARQLWrapper, RDF, JSON
from rest_framework.response import Response
from django.template import RequestContext, loader
import requests
from django.shortcuts import render
from models import NumberGenerators
def index(request):
    template = loader.get_template('index.html')
    return render(request, 'index.html')

def main(request):
    template = loader.get_template('main.html')
    return render(request, 'main.html')
class ProductView(viewsets.ModelViewSet):
    endpoint="http://localhost:8080/openrdf-sesame/repositories/Project"


    @link()
    def list_recipes(self, request, *args, **kwargs):
        result=[]
        name=self.kwargs['name']

        name.replace('%20',' ')
        name=name.lower()
        offset = self.kwargs.get('offset',0)
        endpoint="http://localhost:8080/openrdf-sesame/repositories/Project"
        query=(r"""
            prefix cym: <http://cym.com/rdf-schema#>
            select ?id ?image ?creatorName ?title ?creatorID where{
              ?id a <http://linkedrecipes.org/schema/Recipe> ;
                    <http://purl.org/dc/terms/title> ?title ;
                    <http://xmlns.com/foaf/0.1/depiction> ?image.
              optional {?id cym:creatorName ?creatorName;
                            cym:creatorID ?creatorID}
              FILTER(CONTAINS(LCASE(?title), """+'"'+name+'"'+"""))
            }limit 48""")
        if(offset):
            query+="offset "+str(offset)

        if endpoint and query :
            sparql = SPARQLWrapper(endpoint)
            sparql.setQuery(query)
            sparql.setReturnFormat(JSON)
            sparql.addCustomParameter('Accept','application/sparql-results+json')
            try :
                response_data = sparql.query().convert()
                for i in range(len(response_data['results']['bindings'])):
                    r={}
                    for key in response_data['results']['bindings'][i].keys():
                        r[key]=response_data['results']['bindings'][i][key]['value']
                    result.append(r)

                return Response(result, status=200)
            except Exception as e:
                return Response({'result': e})
        else :
            return Response({'result': 'Error'})
    @link()
    def list_friends_recipes(self, request, *args, **kwargs):
        result=[]
        name=request.DATA.get('name','')
        friends=request.DATA.get('friends')
        # friends=['id1']
        friends=' '.join(friends)
        offset = self.kwargs.get('offset',0)
        endpoint="http://localhost:8080/openrdf-sesame/repositories/Project"

        query=(r"""
            prefix cym: <http://cym.com/rdf-schema#>
            select ?id ?image ?creatorName ?title ?description ?creatorID ?creatorName where{
              ?id a <http://linkedrecipes.org/schema/Recipe> ;
                    <http://purl.org/dc/terms/title> ?title ;
                    <http://xmlns.com/foaf/0.1/depiction> ?image.
              optional {?id cym:creatorName ?creatorName;
                            cym:creatorID ?creatorID}
              optional {?id cym:recipeDescription ?description }
              FILTER(CONTAINS(LCASE(?title), """+'"'+name+'"'+"""))
              FILTER(CONTAINS("""+'"'+friends+'"'+""", ?creatorID))
            }limit 48
             """
        )
        if(offset):
            query+="offset "+str(offset)
        if endpoint and query :
            sparql = SPARQLWrapper(endpoint)
            sparql.setQuery(query)
            sparql.setReturnFormat(JSON)
            sparql.addCustomParameter('Accept','application/sparql-results+json')
            try :
                response_data = sparql.query().convert()
                for i in range(len(response_data['results']['bindings'])):
                    r={}
                    for key in response_data['results']['bindings'][i].keys():
                        r[key]=response_data['results']['bindings'][i][key]['value']
                    result.append(r)

                return Response(result, status=200)
            except Exception as e:
                return Response({'result': e})
        else :
            return Response({'result': 'Error'})

    def get_ingredients(self,product):

        endpoint="http://localhost:8080/openrdf-sesame/repositories/Project"
        #product="<http://data.kasabi.com/dataset/foodista/recipe/ZZ6HPCFF>"
        #(Id, Name, Picture, amountValue, amountMeasure:AmountMeasure, cheapestFullPrice,priceInRecipe,StoreID,StoreName)
        query=("""
            PREFIX cym: <http://cym.com/rdf-schema#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            select distinct ?id ?label ?ingAmount ?productName ?ingAmountMeasure ?image ?amount ?amountMeasure ?cheapestFullPrice      ?store
            where{
              """+product+""" cym:usesIngredient
            ?tempid.
              ?tempid cym:ingredient ?id .



              ?id rdfs:label ?label;
                  <http://xmlns.com/foaf/0.1/depiction> ?image.
               {
              select ?id2 ?measure2 (MIN(?price2 / ?amount2) as ?minprice)
              where{
                """+product+""" cym:usesIngredient
            ?tempID.
                ?tempID cym:ingredient ?id2 .
                ?id2 cym:availableAt ?place2 .
                ?place2 cym:price ?price2 ;
                cym:amountMeasure ?measure2 ;

                cym:amount ?amount2 .
                }group by ?id2 ?measure2

                }


                optional {

                         ?id cym:availableAt ?place .
                         ?place cym:amount ?amount ;
                                cym:amountMeasure ?amountMeasure;
                                cym:price ?cheapestFullPrice;
                                cym:store ?store ;
                                cym:productName ?productName .
                                FILTER(?id2=?id).
                            }

                optional{
              ?tempid cym:amount ?ingAmount .
              ?tempid cym:amountMeasure ?ingAmountMeasure .

              }
                FILTER (( ?ingAmountMeasure = ?amountMeasure ) || (NOT EXISTS { ?tempid cym:amount [] }) ).

                FILTER( (?cheapestFullPrice / ?amount) = ?minprice).




                   optional {?id <http://purl.org/dc/terms/description> ?description .}
             }order by ?label
        """)
        if endpoint and query :
            sparql = SPARQLWrapper(endpoint)

            sparql.setQuery(query)

            sparql.setReturnFormat(JSON)
            sparql.addCustomParameter('Accept','application/sparql-results+json')
            try :

                result=[]
                response_data = sparql.query().convert()
                for i in range(len(response_data['results']['bindings'])):
                    r={}
                    for key in response_data['results']['bindings'][i].keys():
                        r[key]=response_data['results']['bindings'][i][key]['value']
                    result.append(r)
                return result
            except Exception as e:
                return Response({'result': e})


    @link()
    def recipe_details(self,request, *args, **kwargs):
        endpoint="http://localhost:8080/openrdf-sesame/repositories/Project"

        product="<"+request.DATA['qid']+">"
        product.replace('%20',' ')

        query=(r"""
                prefix cym: <http://cym.com/rdf-schema#>
                select ?image ?title ?creator ?description where{
                    """+product+""" a <http://linkedrecipes.org/schema/Recipe> ;
                    <http://purl.org/dc/terms/title> ?title ;
                    <http://xmlns.com/foaf/0.1/depiction> ?image.
                    optional {"""+product+""" cym:recipeDescription ?description }
                    optional {"""+product+""" cym:creatorName ?creator }.
            }
            """)

        if endpoint and query :
            sparql = SPARQLWrapper(endpoint)
            sparql.setQuery(query)
            sparql.setReturnFormat(JSON)
            sparql.addCustomParameter('Accept','application/sparql-results+json')

            try :
                result={}
                response_data = sparql.query().convert()

                for key in response_data['results']['bindings'][0].keys():
                    result[key]=response_data['results']['bindings'][0][key]['value']


                result['ingredients']=self.get_ingredients(product)
                #print response_data['results']['bindings']
                #return Response(response_data['results']['bindings'])
                return Response(result)
            except Exception as e:
                return Response({'result': e})
        else :
            return Response({'result': 'Error'})

        return Response(result)
    @link()
    def list_products(self,request, *args, **kwargs):
        endpoint="http://localhost:8080/openrdf-sesame/repositories/Project"
        query=(r"""
        prefix cym: <http://cym.com/rdf-schema#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        select ?id ?description ?image ?label where{
             ?id ?p ?o;
                a <http://linkedrecipes.org/schema/Food>;
                <http://purl.org/dc/terms/description> ?description;
                rdfs:label ?label;
                <http://xmlns.com/foaf/0.1/depiction> ?image.
                optional {?id cym:creatorName ?creator} .
        }
        limit 20
            """)
        if endpoint and query :
            sparql = SPARQLWrapper(endpoint)
            sparql.setQuery(query)
            sparql.setReturnFormat(JSON)
            sparql.addCustomParameter('Accept','application/sparql-results+json')
            try :
                result=[]
                response_data = sparql.query().convert()
                for i in range(len(response_data['results']['bindings'])):
                    r={}
                    for key in response_data['results']['bindings'][i].keys():
                        r[key]=response_data['results']['bindings'][i][key]['value']
                    result.append(r)
                return Response(result, status=200)
            except Exception as e:
                return Response({'result': e})
        else :
            return Response({'result': 'Error'})

    @link()
    def create_recipe(self,request, *args, **kwargs):
        ingredients=request.DATA['ingredients']
        title=request.DATA.get('name',"")
        #import pdb;pdb.set_trace()
        description = request.DATA.get('description',"")
        image=request.DATA.get('image',"")
        image='"'+image+'"'
        userFacebookID=str(request.DATA.get('userFacebookID',''))
        userFacebookName=request.DATA.get('userFacebookName','').encode('utf-8')
        ng=NumberGenerators.objects.get(id=1)
        id=ng.recipeID
        ng.recipeID+=1
        ng.save()
        title='\"'+title+'\"'
        description='''"'''+description+'''"'''
        recipeID="cym:userRecipes"+str(id)
        data="@prefix cym: <http://cym.com/rdf-schema#>. @prefix xsd: <http://www.w3.org/2001/XMLSchema#> . "
        data += recipeID + " a <http://linkedrecipes.org/schema/Recipe> . "
        data += recipeID + ' <http://purl.org/dc/terms/title> '+title+' . '
        data += recipeID + ' cym:recipeDescription '+description+' . '
        data += recipeID + " <http://xmlns.com/foaf/0.1/depiction> " + image + " . "
        data = data.encode('utf-8') + recipeID + ' cym:creatorName "' + userFacebookName + '"^^xsd:string . '
        data += recipeID + ' cym:creatorID "' + userFacebookID + '"^^xsd:string . '
        for ingredient in ingredients:
            data += recipeID + " cym:usesIngredient [ cym:ingredient <" + ingredient['productID'].encode('utf-8') + ">"
            amV=ingredient.get("amountValue",None)
            if amV:
                data+=' ; cym:amount "'+amV.encode('utf-8')+'"^^xsd:decimal'
            amm=ingredient.get('amountMeasure',None)
            if amm:
                data+=' ; cym:amountMeasure "' + amm.lower().encode('utf-8')+'"^^xsd:string'
            data+= " ] ."
        print data
        url = self.endpoint + "/statements"
        headers = {'content-type': 'application/x-turtle'}
        response = requests.post(url, data=data, headers=headers)

        return Response(True,status=200)

    @link()
    def create_product(self,request, *args, **kwargs):
        description = request.DATA.get('description',"")
        # image="<http://test_im>"#request.DATA.get('image',"")
        label= request.DATA.get('label',"")
        ng=NumberGenerators.objects.get(id=1)
        id=ng.productID
        ng.productID+=1
        ng.save()
        data="@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> . @prefix xsd: <http://www.w3.org/2001/XMLSchema#> . "
        data+="@prefix cym: <http://cym.com/rdf-schema#>. "
        data+="@prefix xsd: <http://www.w3.org/2001/XMLSchema#> . "

        productID="cym:food"+str(id)

        data += productID + " a <http://linkedrecipes.org/schema/Food> . "
        data += productID + ' cym:description "'+description+'"^^xsd:string . '
        # data += productID + " <http://xmlns.com/foaf/0.1/depiction> " + image + " . "
        data += productID + ' rdfs:label "' + label + '"^^xsd:string . '

        url = self.endpoint + "/statements"
        headers = {'content-type': 'application/x-turtle'}
        response = requests.post(url, data=data, headers=headers)
        print data
        return Response(data,status=200)
        
    @link()
    def add_price(self,request, *args, **kwargs):
        print 'asdasdasd'
        id=request.DATA.get('id',"")
        store=request.DATA.get('store',"")
        price=request.DATA.get('price',"")
        amount=request.DATA.get('amount',"")
        amountMeasure=request.DATA.get('amountMeasure','')

        data="""@prefix cym: <http://cym.com/rdf-schema#> . @prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
        <"""+id+"""> cym:availableAt
        [ cym:store """+'"'+store+'"'+ """^^xsd:string ;
          cym:price """+'"'+price+'"'+"""^^xsd:decimal ;
          cym:amount """+'"'+amount+'"'+"""^^xsd:decimal ;
          cym:amountMeasure """+'"'+amountMeasure+'"'+ """^^xsd:string ] ."""
        url = self.endpoint + "/statements"
        headers = {'content-type': 'application/x-turtle'}
        print data
        response = requests.post(url, data=data, headers=headers)
        # print data
        return Response(data,status=200)
        
    @link()
    def list_store_locations(self,request, *args, **kwargs):
        endpoint="http://linkedgeodata.org/sparql/"
        query=(r"""
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX lgdo: <http://linkedgeodata.org/ontology/>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        Prefix geom: <http://geovocab.org/geometry#>
        Prefix ogc: <http://www.opengis.net/ont/geosparql#>
        SELECT distinct ?shopGeo From <http://linkedgeodata.org> {
            ?shop a lgdo:Supermarket;
            rdfs:label ?shopLabel ;
            geom:geometry [
            ogc:asWKT ?shopGeo] .
        Filter ( bif:st_intersects (?shopGeo, bif:st_point (4.8986167, 52.3747157), 10) && ( ?shopLabel = "AH" || ?shopLabel = "Albert Heijn")) .
        }""")
        sparql = SPARQLWrapper(endpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        sparql.addCustomParameter('Accept','application/sparql-results+json')
        import re
        try :
            result=[]
            response_data = sparql.query().convert()
            for i in range(len(response_data['results']['bindings'])):
                r={}
                for key in response_data['results']['bindings'][i].keys():
                    iter = re.finditer("(?P<long>\d+.\d+)\s+(?P<lat>\d+.\d+)", response_data['results']['bindings'][i][key]['value'])
                    m = iter.next()
                    r['long'] = float(m.group('long'))
                    r['lat'] = float(m.group('lat'))
                result.append(r)
            return Response(result, status=200)
        except Exception as e:
            return Response({'result': e})

    @link()
    def list_products_search(self,request, *args, **kwargs):
        endpoint="http://localhost:8080/openrdf-sesame/repositories/Project"
        name=self.kwargs['name']

        name.replace('%20',' ')
        name=name.lower()
        query=(r"""
        prefix cym: <http://cym.com/rdf-schema#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        select ?id ?description ?image ?label where{
             ?id ?p ?o;
                a <http://linkedrecipes.org/schema/Food>;
                <http://purl.org/dc/terms/description> ?description;
                rdfs:label ?label;
                <http://xmlns.com/foaf/0.1/depiction> ?image.
                optional {?id cym:creatorName ?creator} .
        FILTER(CONTAINS(LCASE(?label), """+'"'+name+'"'+"""))
        }
        limit 50
            """)
        if endpoint and query :
            sparql = SPARQLWrapper(endpoint)
            sparql.setQuery(query)
            sparql.setReturnFormat(JSON)
            sparql.addCustomParameter('Accept','application/sparql-results+json')
            try :
                result=[]
                response_data = sparql.query().convert()
                for i in range(len(response_data['results']['bindings'])):
                    r={}
                    for key in response_data['results']['bindings'][i].keys():
                        r[key]=response_data['results']['bindings'][i][key]['value']
                    result.append(r)
                return Response(result, status=200)
            except Exception as e:
                return Response({'result': e})
        else :
            return Response({'result': 'Error'})