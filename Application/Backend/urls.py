__author__ = 'anon'
from django.contrib import admin
from django.conf.urls import patterns ,url
import views
from django.conf.urls.static import static
from Application import settings
admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^getRecipes/(?P<name>.+)/(?P<offset>\d*)',views.ProductView.as_view({'get':'list_recipes'})),
                       url(r'^getRecipes/(?P<name>.+)$',views.ProductView.as_view({'get':'list_recipes'})),
                       #url(r'getRecipe/(?P<name>.+)',views.ProductView.as_view({'get':'list_recipes'})),
                       url(r'getRecipe/',views.ProductView.as_view({'post':'recipe_details'})),
                       url(r'getAllProducts/',views.ProductView.as_view({'get':'list_products'})),
                       url(r'searchProducts/(?P<name>.+)',views.ProductView.as_view({'get':'list_products_search'})),
                       url(r'createrecipe',views.ProductView.as_view({'post':'create_recipe'})),
                       url(r'createrecipe/',views.ProductView.as_view({'post':'create_recipe'})),
                       url(r'^postProductPrice/$',views.ProductView.as_view({'post':'add_price'})),
                       url(r'postProduct',views.ProductView.as_view({'post':'create_product'})),
                       url(r'postProduct/',views.ProductView.as_view({'post':'create_product'})),
                       url(r'^getFriendsRecipes/(?P<offset>\d*)$',views.ProductView.as_view({'post':'list_friends_recipes'})),

                       url(r'getStoreLocations/',views.ProductView.as_view({'get':'list_store_locations'})),
                       url(r'^hello/', views.index, name='index'),
                       url(r'^main/', views.main, name='main'),
                       )+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)