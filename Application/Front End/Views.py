__author__ = 'anon'

from rest_framework import viewsets
from rest_framework.decorators import link
from SPARQLWrapper import SPARQLWrapper, RDF, JSON
from rest_framework.response import Response
import requests
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.shortcuts import render

def index(request):
    template = loader.get_template('index.html')
    return render(request, 'index.html')

def main(request):
    template = loader.get_template('main.html')
    return render(request, 'main.html')
    
class ProductView(viewsets.ModelViewSet):
    endpoint="http://localhost:8080/openrdf-sesame/repositories/Project"
    @link()
    def list_recipes(self, request, *args, **kwargs):
        # result=[]
        # name=self.kwargs['name']
        # endpoint="http://localhost:8080/openrdf-sesame/repositories/Project"
        # query=(r"""
        #     prefix cym: <http://cym.com/rdf-schema#>
        #     select ?id ?image ?creatorName ?title ?creatorID where{
        #       ?id a <http://linkedrecipes.org/schema/Recipe> ;
        #             <http://purl.org/dc/terms/title> ?title ;
        #             <http://xmlns.com/foaf/0.1/depiction> ?image.
        #       optional {?id cym:creatorName ?creatorName;
        #                     cym:creatorID ?creatorID}
        #       FILTER(CONTAINS(?title, """+'"'+name+'"'+"""))
        #     }limit 20
        #     """)
        # if endpoint and query :
        #     sparql = SPARQLWrapper(endpoint)
        #     sparql.setQuery(query)
        #     sparql.setReturnFormat(JSON)
        #     sparql.addCustomParameter('Accept','application/sparql-results+json')
        #     try :
        #         response_data = sparql.query().convert()
        #         for i in range(len(response_data['results']['bindings'])):
        #             r={}
        #             for key in response_data['results']['bindings'][i].keys():
        #                 r[key]=response_data['results']['bindings'][i][key]['value']
        #             result.append(r)
        #
        #         return Response(result, status=200)
        #     except Exception as e:
        #         return Response({'result': e})
        # else :
        #     return Response({'result': 'Error'})
        recipe_list=[
    {
        "image": "http://cloud.foodista.com/content/images/3775aae03cf2387cced065b8e47c31c379406eef_607x400.jpg",
        "title": "Carrot Pineapple Moist Cake",
        "creatorName": "name3",
        "creatorID": "id3",
        "id": "http://data.kasabi.com/dataset/foodista/recipe/ZW6VH8S8"
    },
    {
        "image": "http://cloud.foodista.com/content/images/95e803ad66f78ebc6973268c7cac3ec66f73261a_607x400.jpg",
        "id": "http://data.kasabi.com/dataset/foodista/recipe/ZZ4Z2483",
        "title": "Pineapple-Infused Vodka"
    },
    {
        "image": "http://cloud.foodista.com/content/images/61a46ddbb61cb2b6cf6e08a7f5a908afafc25e47_607x400.jpg",
        "id": "http://data.kasabi.com/dataset/foodista/recipe/ZWLPFTV6",
        "title": "Buttery Pineapple Cake"
    },
    {
        "image": "http://cloud.foodista.com/content/images/d467e37b569712b13baee7fdeb22ab661b76095e_607x400.jpg",
        "title": "Pineapple Vinegar",
        "creatorName": "name1",
        "creatorID": "id1",
        "id": "http://data.kasabi.com/dataset/foodista/recipe/ZVM76FGH"
    },]
        return Response(recipe_list)
    @link()
    def list_friends_recipes(self, request, *args, **kwargs):
        # result=[]
        # name='apple'#request.DATA.get('name','')
        # #friends=request.DATA.get('friends')
        # friends=['id1']
        # friends=' '.join(friends)
        #
        # endpoint="http://localhost:8080/openrdf-sesame/repositories/Project"
        #
        # query=(r"""
        #     prefix cym: <http://cym.com/rdf-schema#>
        #     select ?id ?image ?creatorName ?title ?creatorID ?creatorName where{
        #       ?id a <http://linkedrecipes.org/schema/Recipe> ;
        #             <http://purl.org/dc/terms/title> ?title ;
        #             <http://xmlns.com/foaf/0.1/depiction> ?image.
        #       optional {?id cym:creatorName ?creatorName;
        #                     cym:creatorID ?creatorID}
        #       FILTER(CONTAINS(?title, """+'"'+name+'"'+"""))
        #       FILTER(CONTAINS("""+'"'+friends+'"'+""", ?creatorID))
        #     }limit 20
        #     """)
        # if endpoint and query :
        #     sparql = SPARQLWrapper(endpoint)
        #     sparql.setQuery(query)
        #     sparql.setReturnFormat(JSON)
        #     sparql.addCustomParameter('Accept','application/sparql-results+json')
        #     try :
        #         response_data = sparql.query().convert()
        #         for i in range(len(response_data['results']['bindings'])):
        #             r={}
        #             for key in response_data['results']['bindings'][i].keys():
        #                 r[key]=response_data['results']['bindings'][i][key]['value']
        #             result.append(r)
        #
        #         return Response(result, status=200)
        #     except Exception as e:
        #         return Response({'result': e})
        # else :
        #     return Response({'result': 'Error'})
        result=[
    {
        "image": "http://cloud.foodista.com/content/images/d467e37b569712b13baee7fdeb22ab661b76095e_607x400.jpg",
        "id": "http://data.kasabi.com/dataset/foodista/recipe/ZVM76FGH",
        "creatorID": "id1",
        "creatorName":"creator1",
        "title": "Pineapple Vinegar"
    },
    {
        "image": "http://cloud.foodista.com/content/images/d467e37b569712b13baee7fdeb22ab661b76095e_607x400.jpg",
        "id": "http://data.kasabi.com/dataset/foodista/recipe/ZVM76FGH",
        "creatorID": "id2",
        "creatorName":"creator2",
        "title": "Pineapple Vinegar2"
    }
]
        return Response(result)


    def get_ingredients(self,request, *args, **kwargs):
        endpoint="http://localhost:8080/openrdf-sesame/repositories/Project"
        product="<http://data.kasabi.com/dataset/foodista/recipe/ZZ6HPCFF>"
        #(Id, Name, Picture, amountValue, amountMeasure:AmountMeasure, cheapestFullPrice,priceInRecipe,StoreID,StoreName)
        query=("""
        PREFIX cym: <http://cym.com/rdf-schema#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        select ?id ?label ?image ?amount ?amountMeasure ?cheapestFullPrice ?store where{
        """+product+"""  <http://linkedrecipes.org/schema/ingredient> ?id.
                ?id rdfs:label ?label;
                    <http://xmlns.com/foaf/0.1/depiction> ?image.
                    {
                    select ?id2 (MIN(?price2 / ?amount2) as ?minprice)
                    where{
                        """+product+"""  <http://linkedrecipes.org/schema/ingredient> ?id2.
                        ?id2 cym:availableAt ?place2 .
                        ?place2 cym:price ?price2 ;
                            cym:amount ?amount2 .
                    }group by ?id2
                    }
                    optional {?id cym:availableAt ?place .
                              ?place cym:amount ?amount ;
                                     cym:amountMeasure ?amountMeasure;
                                     cym:price ?cheapestFullPrice;
                                     cym:store ?store .

                              FILTER( (?cheapestFullPrice / ?amount) = ?minprice).


                              }
                    FILTER(?id2=?id).
                    optional {?id <http://purl.org/dc/terms/description> ?description;}
                }
        """)
        if endpoint and query :
            sparql = SPARQLWrapper(endpoint)

            sparql.setQuery(query)

            sparql.setReturnFormat(JSON)
            sparql.addCustomParameter('Accept','application/sparql-results+json')
            try :

                result=[]
                response_data = sparql.query().convert()
                for i in range(len(response_data['results']['bindings'])):
                    r={}
                    for key in response_data['results']['bindings'][i].keys():
                        r[key]=response_data['results']['bindings'][i][key]['value']
                    result.append(r)
                return result
            except Exception as e:
                return Response({'result': e})




    @link()
    def recipe_details(self,product):
        # endpoint="http://localhost:8080/openrdf-sesame/repositories/Project"
        #
        # # id : <http://data.kasabi.com/dataset/foodista/recipe/2262RV7P> the actual ?s itself
        # # image : <http://xmlns.com/foaf/0.1/depiction>
        # # title : <http://purl.org/dc/terms/title>
        # # recipe : <http://linkedrecipes.org/schema/Recipe>
        # query=(r"""
        #         PREFIX cym: <http://localhost/>
        #         select ?image ?title ?creator where{
        #             <http://data.kasabi.com/dataset/foodista/recipe/ZZX62GBC> a <http://linkedrecipes.org/schema/Recipe> ;
        #             <http://purl.org/dc/terms/title> ?title ;
        #             <http://xmlns.com/foaf/0.1/depiction> ?image.
        #
        #              optional {?id cym:creatorName ?creator }.
        #     }limit 20
        #     """)
        #
        #
        #
        # if endpoint and query :
        #     sparql = SPARQLWrapper(endpoint)
        #
        #     sparql.setQuery(query)
        #
        #     sparql.setReturnFormat(JSON)
        #     sparql.addCustomParameter('Accept','application/sparql-results+json')
        #
        #     try :
        #         result={}
        #         response_data = sparql.query().convert()
        #
        #         response_data2=sparql.query().convert()
        #
        #         #response_data['head']['vars'].append('ingredients')
        #
        #         #result['vars']=response_data['head']['vars']
        #         #
        #         for key in response_data['results']['bindings'][0].keys():
        #             result[key]=response_data['results']['bindings'][0][key]['value']
        #
        #
        #         result['ingredients']=self.get_ingredients(product)
        #         #print response_data['results']['bindings']
        #         #return Response(response_data['results']['bindings'])
        #         return Response(result)
        #     except Exception as e:
        #         return Response({'result': e})
        # else :
        #     return Response({'result': 'Error'})
        result={
    "image": "http://cloud.foodista.com/content/images/d243ac75e03deb9d1b71b34c0efc5b624904c2aa_607x400.jpg",
    "ingredients": [
        {
            "amountMeasure": "grams",
            "image": "http://cloud.foodista.com/content/images/cb2b2fc48ceb9fdbcf690f3a17977b705f7de0a8_607x400.jpg",
            "label": "Thyme Blossoms- \u0628\u0631\u0627\u0639\u0645 \u0627\u0644\u0632\u0639\u062a\u0631 \u0627\u0644\u0628\u0631\u064a",
            "amount": "15.0",
            "cheapestFullPrice": "1.39",
            "id": "http://data.kasabi.com/dataset/foodista/food/ZLS6DSP3",
            "store": "Albert Heim"
        },
        {
            "amountMeasure": "ml",
            "image": "http://cloud.foodista.com/content/images/aa8e95c41c2ccb0fd4ef84a8222e1eb7cbacc9ba_607x400.jpg",
            "label": "Onion",
            "amount": "485.0",
            "cheapestFullPrice": "1.56",
            "id": "http://data.kasabi.com/dataset/foodista/food/YCPMHNRB",
            "store": "Albert Heim"
        },
        {
            "amountMeasure": "grams",
            "image": "http://cloud.foodista.com/content/images/8a425112be00bd163dbe370ac9272e6274dc60de_607x400.jpg",
            "label": "Salt",
            "amount": "125.0",
            "cheapestFullPrice": "0.29",
            "id": "http://data.kasabi.com/dataset/foodista/food/XBPNFKYS",
            "store": "Albert Heim"
        },
        {
            "amountMeasure": "grams",
            "image": "http://cloud.foodista.com/content/images/a5f2592fdc781220a3064a87e580e9da4d97d07d_607x400.jpg",
            "label": "Black Pepper",
            "amount": "500.0",
            "cheapestFullPrice": "1.39",
            "id": "http://data.kasabi.com/dataset/foodista/food/SWV3CGDQ",
            "store": "Albert Heim"
        },
        {
            "amountMeasure": "grams",
            "image": "http://cloud.foodista.com/content/images/887c19de5e394a82815f68a14eded3644fae6c06_607x400.jpg",
            "label": "Mussel",
            "amount": "200.0",
            "cheapestFullPrice": "2.69",
            "id": "http://data.kasabi.com/dataset/foodista/food/RQZBBMNQ",
            "store": "Albert Heim"
        },
        {
            "amountMeasure": "ml",
            "image": "http://cloud.foodista.com/content/images/836b116e1beaddbc7460dcdbbfe9f08ddaf36aae_607x400.jpg",
            "label": "White Wine",
            "amount": "1000.0",
            "cheapestFullPrice": "0.99",
            "id": "http://data.kasabi.com/dataset/foodista/food/Q67GD8BS",
            "store": "Albert Heim"
        },
        {
            "amountMeasure": "ml",
            "image": "http://cloud.foodista.com/content/images/5bd34b40decda122f60c379a69ec4633a3fce30b_607x400.jpg",
            "label": "Olive Oil",
            "amount": "1000.0",
            "cheapestFullPrice": "1.19",
            "id": "http://data.kasabi.com/dataset/foodista/food/NLPYVN22",
            "store": "Albert Heim"
        },
        {
            "amountMeasure": "ml",
            "image": "http://cloud.foodista.com/content/images/21a5ab0ae99a21a125fbbbaae754810e95ee2839_607x400.jpg",
            "label": "Blue Solaize Leeks",
            "amount": "3000.0",
            "cheapestFullPrice": "2.05",
            "id": "http://data.kasabi.com/dataset/foodista/food/MKBY8FVC",
            "store": "Albert Heim"
        },
        {
            "amountMeasure": "grams",
            "image": "http://cloud.foodista.com/content/images/dc041bc4a31bd2f57a89b3b8ac70753f96af4aab_607x400.jpg",
            "label": "Oregano",
            "amount": "200.0",
            "cheapestFullPrice": "3.49",
            "id": "http://data.kasabi.com/dataset/foodista/food/LC2JZ743",
            "store": "Albert Heim"
        },
        {
            "amountMeasure": "grams",
            "image": "http://cloud.foodista.com/content/images/345974137d222559ef62feab62407c04da965107_607x400.jpg",
            "label": "Parsley",
            "amount": "30.0",
            "cheapestFullPrice": "1.0",
            "id": "http://data.kasabi.com/dataset/foodista/food/GKXHVZLV",
            "store": "Albert Heim"
        },
        {
            "amountMeasure": "ml",
            "image": "http://cloud.foodista.com/content/images/1708ac6de067950c74b03d9a3bbe94c309eb6813_607x400.jpg",
            "label": "Garlic",
            "amount": "390.0",
            "cheapestFullPrice": "1.39",
            "id": "http://data.kasabi.com/dataset/foodista/food/FQRPFPP6",
            "store": "Albert Heim"
        },
        {
            "amountMeasure": "grams",
            "image": "http://cloud.foodista.com/content/images/42b3b153a1d490b73f4b4e250877244ac32f5b3b_607x400.jpg",
            "label": "Tomato",
            "amount": "68.0",
            "cheapestFullPrice": "0.08",
            "id": "http://data.kasabi.com/dataset/foodista/food/8X4J45TL",
            "store": "Albert Heim"
        },
        {
            "amountMeasure": "grams",
            "image": "http://cloud.foodista.com/content/images/b47d5dd5e26bea0440296500d6e722e3e7059281_607x400.jpg",
            "label": "Bay Leaf",
            "amount": "500.0",
            "cheapestFullPrice": "3.99",
            "id": "http://data.kasabi.com/dataset/foodista/food/5PN7S6FY",
            "store": "Albert Heim"
        },
        {
            "amountMeasure": "grams",
            "image": "http://cloud.foodista.com/content/images/89aecff2322ea0934a8ba7e1fbf661fdb5111d15_607x400.jpg",
            "label": "Shrimp",
            "amount": "60.0",
            "cheapestFullPrice": "0.36",
            "id": "http://data.kasabi.com/dataset/foodista/food/54KQFNS4",
            "store": "Albert Heim"
        }
    ],
    "title": "Cuban Stew"
}
        return Response(result)
    @link()
    def list_products(self,request, *args, **kwargs):
        # endpoint="http://localhost:8080/openrdf-sesame/repositories/Project"
        # query=(r"""
        # PREFIX cym: <http://localhost/>
        # PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        # select ?id ?description ?image ?label where{
        #      ?id ?p ?o;
        #         a <http://linkedrecipes.org/schema/Food>;
        #         <http://purl.org/dc/terms/description> ?description;
        #         rdfs:label ?label;
        #         <http://xmlns.com/foaf/0.1/depiction> ?image.
        #         optional {?id cym:creatorName ?creator} .
        # }limit 20
        #     """)
        # if endpoint and query :
        #     sparql = SPARQLWrapper(endpoint)
        #     sparql.setQuery(query)
        #     sparql.setReturnFormat(JSON)
        #     sparql.addCustomParameter('Accept','application/sparql-results+json')
        #     try :
        #         result=[]
        #         response_data = sparql.query().convert()
        #         for i in range(len(response_data['results']['bindings'])):
        #             r={}
        #             for key in response_data['results']['bindings'][i].keys():
        #                 r[key]=response_data['results']['bindings'][i][key]['value']
        #             result.append(r)
        #         return Response(result, status=200)
        #     except Exception as e:
        #         return Response({'result': e})
        # else :
        #     return Response({'result': 'Error'})
        result=[
    {
        "image": "http://cloud.foodista.com/content/misc/placeholders/food_big",
        "label": "Red Ivy Leaf Sweet Potato",
        "description": "The Red Ivy Leaf Sweet Potato Has Pink Skin, Which Is Deep In Color. The Potatos Flesh Is Firm And Orange In Color And Has A Mild, Sweet Flavor. This Type Of Potato Can Be Used In Soups, Stews Or Used In Baked Goods",
        "id": "http://data.kasabi.com/dataset/foodista/food/3C8TYMJV"
    },
    {
        "image": "http://cloud.foodista.com/content/misc/placeholders/food_big",
        "label": "Catalina Island Cherry",
        "description": "A Cherry Native To The Channel Islands Of Southern California. The Fruits Ripen To Red And Are Cherry-like In Appearance And Flavor, Though Not As Sweet",
        "id": "http://data.kasabi.com/dataset/foodista/food/222YXBYC"
    },
    {
        "image": "http://cloud.foodista.com/content/images/6e10b3eb36a13088bb22fe20f1019d0a182f5b95_607x400.jpg",
        "label": "Club Soda",
        "description": "Club Soda Is A Type Of Carbonated Water With A Slightly Salty Taste. Club Soda Is Mixed With Other Drinks Or Added To Recipes To Make Fluffier Cookies, Cakes, Pancakes And Waffles. It Is Also Useful Around The House For Cleaning Porcelain Sinks And Fixtures And Removing Stains From Fabrics",
        "id": "http://data.kasabi.com/dataset/foodista/food/2NLX8VNF"
    },
    {
        "image": "http://cloud.foodista.com/content/images/500dc895c5aea1fb9d1105efc14d8f9e79692802_607x400.jpg",
        "label": "Pristine   Apple",
        "description": "",
        "id": "http://data.kasabi.com/dataset/foodista/food/3YW4H2QG"
    }]
        return Response(result)
    @link()
    def create_recipe(self,request, *args, **kwargs):
        #data = request.DATA.encode('utf-8')
        #([productID,amountValue, amountMeasure:AmountMeasure], description, Image, userFacebookID, userFacebookName)
        ingredients=["<http://ingredient2>"]#request.DATA.get('ingredients')
        description="<http://test_desc2>"#request.DATA.get('description',"")
        image="<http://test_im>"#request.DATA.get('image',"")
        userFacebookID="<http://test_id>"#request.DATA.get('userFacebookID','')
        userFacebookName="<http://test_name>"#request.DATA.get('userFacebookName','')
        id=0
        recipeID="<http://localhost/cym/recipes/"+str(id)+">"

        data  = recipeID + " a <http://linkedrecipes.org/schema/Recipe> ."
        data += recipeID + " <http://purl.org/dc/terms/title> "+description+' .'
        data += recipeID + " <http://xmlns.com/foaf/0.1/depiction>" + image + " ."
        data += recipeID + " <http://localhost/cym/creatorName>" + userFacebookName + " ."
        data += recipeID + " <http://localhost/cym/creatorID>" + userFacebookID + " ."

        for ingredient in ingredients:
            data += recipeID + " <http://linkedrecipes.org/schema/ingredient>" + ingredient + " ."
        print data
        url = self.endpoint + "/statements"
        headers = {'content-type': 'application/x-turtle'}
        response = requests.post(url, data=data, headers=headers)

        return Response(data,status=200)

    @link()
    def create_product(self,request, *args, **kwargs):

        #(label, Description, Picture)

        description="<http://test_desc2>"#request.DATA.get('description',"")
        image="<http://test_im>"#request.DATA.get('image',"")
        label="<http://test_im>"#request.DATA.get('label',"")
        id=1

        data="@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> ."

        productID="<http://localhost/cym/food/"+str(id)+">"

        data += productID + " a <http://linkedrecipes.org/schema/Food> ."
        data += productID + " <http://purl.org/dc/terms/description> "+description+' .'
        data += productID + " <http://xmlns.com/foaf/0.1/depiction>" + image + " ."
        data += productID + " rdfs:label" + label + " ."

        url = self.endpoint + "/statements"
        headers = {'content-type': 'application/x-turtle'}
        response = requests.post(url, data=data, headers=headers)

        return Response(data,status=200)
    @link()
    def add_price(self,request, *args, **kwargs):
        #Post: (ProductID, price, amountValue, amountMeasure:AmountMeasure, store)

        id="<http://data.kasabi.com/dataset/foodista/food/3CJDF2X8>"#request.DATA.get('id',"")
        store="Albert Heijn"#request.DATA.get('store',"")
        price="2"#request.DATA.get('price',"")
        amount="1"#request.DATA.get('amount',"")
        amountMeasure="ml"#request.DATA.get('amountMeasure','')

        data="""@prefix cym: <http://cym.com/rdf-schema#> .
        """+id+""" cym:availableAt
		[ cym:store """+'"'+store+'"'+ """^^xsd:string ;
		  cym:price """+'"'+price+'"'+"""^^xsd:decimal ;
		  cym:amount """+'"'+amount+'"'+"""^^xsd:decimal ;
		  cym:amountMeasure """+'"'+amountMeasure+'"'+ """^^xsd:string ] ."""
        url = self.endpoint + "/statements"
        headers = {'content-type': 'application/x-turtle'}
        response = requests.post(url, data=data, headers=headers)
        print data

        return Response(data,status=200)