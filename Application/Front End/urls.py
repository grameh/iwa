__author__ = 'anon'
from django.contrib import admin
from django.conf.urls import patterns ,url
from mysite import Views
from mysite import settings

from django.conf.urls import patterns, url

from django.conf.urls.static import static
admin.autodiscover()

urlpatterns = patterns('',
                       url(r'getRecipes/(?P<name>.+)',Views.ProductView.as_view({'get':'list_recipes'})),
                       url(r'getRecipe/',Views.ProductView.as_view({'get':'recipe_details'})),
                       url(r'getAllProducts/',Views.ProductView.as_view({'get':'list_products'})),
                       url(r'createrecipe/',Views.ProductView.as_view({'get':'create_recipe'})),
                       url(r'postProduct/',Views.ProductView.as_view({'get':'create_product'})),
                       url(r'^getFriendsRecipes/$',Views.ProductView.as_view({'get':'list_friends_recipes'})),
                       url(r'^postProductPrice/$',Views.ProductView.as_view({'get':'add_price'})),
                        url(r'^hello/', Views.index, name='index'),
    					url(r'^main/', Views.main, name='main'),
                       ) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
