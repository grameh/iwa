from SPARQLWrapper import SPARQLWrapper, JSON

sparql = SPARQLWrapper("http://localhost:8080/openrdf-sesame/repositories/tutorial")
sparql.setQuery("""
            PREFIX lr: <http://linkedrecipes.org/schema/>
            SELECT ?id
            WHERE{ ?id a lr:Recipe }
            """)
sparql.setReturnFormat(JSON)
results = sparql.query().convert()

for result in results["results"]["bindings"]:
    print(result["label"]["value"])