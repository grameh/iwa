import urllib
import urllib2
import re

class Product:
	image = ''
	price = -1
	name = ''
	amount = ''
	id = ''
	reduced_name= ''
	english_name= ''
	reduced_english_name = ''
	def __str__(self):
		return "(" + self.image + "," + str(self.price) + "," + self.name + "," + self.amount + "," + self.id + ")"
	@staticmethod
	def deserialize(string):
		p = Product()
		strlist = string.split("$")
		p.image = strlist[0]
		p.price = float(strlist[1])
		p.name = strlist[2]
		p.amount = strlist[3]
		p.id = strlist[4]
		return p
	def serialize_all(self):
		return self.image+'$'+str(self.price)+'$'+self.name+'$'+self.amount+'$'+self.id+'$'+self.reduced_name+'$'+self.english_name+'$'+self.reduced_english_name#).encode('utf8')
	@staticmethod
	def deserialize_all(string):
		p = Product()
		strlist = string.split("$")
		p.image = strlist[0]
		p.price = float(strlist[1])
		p.name = strlist[2]
		p.amount = strlist[3]
		p.id = strlist[4]
		p.reduced_name = strlist[5]
		p.english_name = strlist[6]
		p.reduced_english_name = strlist[7]
		return p
		
_digits = re.compile('\d')
def contains_digits(d):
    return bool(_digits.search(d))
	
pages = ['http://www.ah.nl/appie/producten/aardappel-groente-fruit'
,'http://www.ah.nl/appie/producten/verse-kant-en-klaar-maaltijden-salades'
,'http://www.ah.nl/appie/producten/vlees-kip-vis-vega'
,'http://www.ah.nl/appie/producten/kaas-vleeswaren-delicatessen'
,'http://www.ah.nl/appie/producten/zuivel-eieren'
,'http://www.ah.nl/appie/producten/bakkerij'
,'http://www.ah.nl/appie/producten/ontbijtgranen-broodbeleg-tussendoor'
,'http://www.ah.nl/appie/producten/frisdrank-sappen-koffie-thee'
,'http://www.ah.nl/appie/producten/wijn-bier-sterke-drank'
,'http://www.ah.nl/appie/producten/pasta-rijst-internationale-keuken'
,'http://www.ah.nl/appie/producten/soepen-conserven-sauzen-smaakmakers'
,'http://www.ah.nl/appie/producten/snoep-koek-chips'
,'http://www.ah.nl/appie/producten/diepvries'
#,'http://www.ah.nl/appie/producten/baby'
,'http://www.ah.nl/appie/producten/drogisterij'
,'http://www.ah.nl/appie/producten/speciale-voeding']
# ,'http://www.ah.nl/appie/producten/huishouden-huisdier'
# ,'http://www.ah.nl/appie/producten/koken-tafelen-non-food']

# parserstring = "<div class=\"canvas_card product draggable\"\s+data-appie=\"productpreview\"\s*>\s*<div class=\"image\">\s*<img src=\"(?P<imgref>[^\"]*)\" alt=\"(?P<name>[^\"]*)\">\s*(<div class=\"shield shield-beta shield-bonus shield-beta\">\s*<span class='shield-text-alpha'>\s*BONUS\s*</span>\s*</div>\s*|<div class=\"shield shield-beta shield-icon shield--beta\">\s*</div>)?\s*<p class=\"unit\">\s*(?P<amount>[^<]*)\s*</p>\s*((<p class=\"price\">\s*<ins>\s*(?P<price1>\d+\.<span>\d+)|<p class=\"price bonus\">\s*<del>\s*\d+\.\d+\s*</del>\s*<ins>\s*(?P<price2>\d+\.<span>\d+))\s*</span>\s*</ins>\s*</p>)?\s*</div>(.*?)wi(?P<id>\d+)"
# pageind = 1
# for page in pages:
	# productCatPage = urllib.urlopen(page).read()
	# iter = re.finditer("(?P<id>wi\d+)", productCatPage)
	# prodListBase = "http://www.ah.nl/appie/vind/productlist?skipSitestat=true&originCode=CAT&productList="
	# prodListUrl = prodListBase
	# for m in iter:	
		# prodListUrl = prodListUrl + m.group("id") + ","
	# prodListUrl = prodListUrl[:-1]
	# productsPage = urllib.urlopen(prodListUrl).read().decode('iso-8859-1')#.encode('utf8')

	# iter = re.finditer(parserstring, productsPage, re.DOTALL)
	# products = []
	# for m in iter:
		# p = Product()
		# p.image = m.group("imgref")
		# p.name = m.group("name")
		# p.amount = m.group("amount").strip()
		# p.id = "wi" + m.group("id")
		# if m.group("price1"):
			# p.price = float(m.group("price1").replace("<span>", ""))
		# elif m.group("price2"):
			# p.price = float(m.group("price2").replace("<span>", ""))
		# products.append(p)
		# print m.groupdict()

	# iter = re.finditer("productTaxonomyLevelId=(?P<taxonomyId>\d+)", productCatPage)
	# prodTaxonomyBase = "http://www.ah.nl/appie/vind/product?skipSitestat=true&originCode=CAT&productTaxonomyLevelId="
	# w = 1
	# for m in iter:
		# productsPage = urllib.urlopen(prodTaxonomyBase+m.group("taxonomyId")).read().decode('iso-8859-1')#.encode('utf8')
		# iterinner = re.finditer(parserstring, productsPage, re.DOTALL)
		# if w==0:
			# f = open("productsPage.html", "w")
			# f.write(productsPage)
			# f.close()
		# w+=1
		# for product in iterinner:
			# p = Product()
			# p.image = product.group("imgref")
			# p.name = product.group("name").replace("Kilo'tje", "Kilo").replace("kilo'tje", "kilo")
			# p.amount = product.group("amount").strip()
			# p.id = "wi" + product.group("id")
			# if product.group("price1"):
				# p.price = float(product.group("price1").replace("<span>", ""))
			# elif product.group("price2"):
				# p.price = float(product.group("price2").replace("<span>", ""))
			# products.append(p)
			
	# f = open('AHdata' + str(pageind) + '.txt', 'w')
	# for product in products:
		# f.write(product.serialize()+'\n')
	# f.close()
	# pageind += 1

# import goslate
# gs = goslate.Goslate()
# pageind = 1
# for ind in range(1, 18)
	# f = open('AHdata' + str(pageind) + '_en.txt', 'w')
	# with open('AHdata' + str(pageind) + '.txt', 'r') as openfileobject:
		# for line in openfileobject:
			# line = line[:-1]
			# product = Product.deserialize(line)
			# product.name = gs.translate(product.name, 'en', 'nl')
			# f.write(product.serialize()+"\n")
	# f.close()

# from sets import Set
# nameSet = Set()
# tagSet = Set()
# names = []
# products = []
# import goslate
# gs = goslate.Goslate()
# for ind in range(1, 16):
	# with open('AHdata' + str(ind) + '.txt', 'r') as openfileobject:
		# for line in openfileobject:
			# line = line[:-1]
			# product = Product.deserialize(line)
			# name = re.sub('[(),]', ' ', product.name)
			# nametags = name.split()
			# name = ''
			# for tag in nametags:
				# if not contains_digits(tag):
					# name += tag + ' '
			# name = re.sub('[/+-]', ' ', name).strip()
			# product.reduced_name = name
			# print ind
			# product.english_name = gs.translate(product.name, 'en', 'nl').encode('utf8')
			# product.reduced_english_name = 'asdf'
			# serd = product.serialize_all()
			# products.append(product)
			# names.append(name)
			# nameSet.add(name)
			# tagSet = tagSet | Set(name.split())

# f = open('AHdataALL.txt', 'w')
# for product in products:
	# f.write(product.serialize_all()+'\n')
# f.close()
			
# print len(names)
# print len(nameSet)
# f = open("all_names.txt", "w")
# for name in nameSet:
	# if name[:3] == 'AH ':
		# f.write(name[3:]+'\n')
	# else:
		# f.write(name+'\n')		
# f.close()

# def transform_amount(amount):
	# prep = re.sub('gr$|g$|gram$', 'grams', re.sub('....nkops|zakjes', 'filters', amount.replace('ca.', '').replace(',','.'))).replace(' x ', 'x').replace(' X ', 'x')
	# if prep == 'per stuk' or prep == 'per bosje':
		# return 1, 'pieces'
	# else:
		# st = prep.split()
		# if 'x' in st[0]:
			# if st[0].endswith('x'):
				# num = float(st[0][:-1])
			# else:
				# stnum = st[0].split('x')
				# num = float(stnum[0]) * float(stnum[1])
		# else:
			# num = float(st[0])
		# measure = st[1]
		# if measure == 'kgrams':
			# measure = 'grams'
			# num *= 1000
		# elif measure == 'cl':
			# measure = 'ml'
			# num *= 10
		# elif measure == 'lt':
			# measure = 'ml'
			# num *= 1000
		# elif measure == 'stuks' or measure == 'Stuks' or measure == 'stuk':
			# measure = 'pieces'
		# print amount + '\t\t\t\t: ' + str(num) + ' ' + measure
		# # return prep
		# # return str(num) + ' ' + measure
		# return num, measure

# from sets import Set
# amountSet = Set()
# f = open('AHdataV2.txt', 'w')
# with open('AHdataV1.txt', 'r') as openfileobject:
	# for line in openfileobject:
		# line = line[:-1]
		# product = Product.deserialize_all(line)
		# # amount = product.amount.replace('ca.', '')
		# num, measure = transform_amount(product.amount)
		# amount = str(num) + ' ' + measure
		# product.amount = amount
		# f.write(product.serialize_all() + '\n')
		# amount = re.sub('\d+|[,\.]', '', amount)
		# amountSet.add(' '.join(amount.split()))
# f.close()

# idset = {}
# f = open('AHdataV3.txt', 'w')
# with open('AHdataV2.txt', 'r') as openfileobject:
	# for line in openfileobject:
		# product = Product.deserialize_all(line)
		# if not product.id in idset:
			# idset[product.id] = 1
			# f.write(line)
# f.close()		


# f = open('AHdataV4.txt', 'w')
# with open('AHdataV3.txt', 'r') as openfileobject:
	# for line in openfileobject:
		# product = Product.deserialize_all(line)
		# if product.price >= 0:
			# f.write(line)
# f.close()


# f = open('amounts.txt', 'w')
# for amount in amountSet:
	# f.write(amount+'\n')
# f.close()




from sets import Set
nameSet = Set()
tagSet = Set()
names = []
products = []
with open('AHdataV4.txt', 'r') as openfileobject:
	for line in openfileobject:
		line = line[:-1]
		product = Product.deserialize_all(line)
		name = re.sub('[(),#&]', ' ', product.english_name)
		nametags = name.split()
		name = ''
		for tag in nametags:
			if not contains_digits(tag):
				name += tag + ' '
		name = re.sub('[/+-]', ' ', name).strip()
		product.reduced_english_name = ' '.join(name.split())
		products.append(product)
		names.append(name)
		nameSet.add(name)
		tagSet = tagSet | Set(name.split())

tags = sorted(list(tagSet))
tagsFinal = []
for tag in tags:
	t = tag.strip()
	if len(t) >= 3:
		tagsFinal.append(t)

ttlfile = open('ahV4.ttl', 'w')
ttlfile.write('@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .\n')
ttlfile.write('@prefix cym: <http://cym.com/rdf-schema#> .\n')
ttlfile.write('@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n')
ttlfile.write('@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n')
ttlfile.write('@prefix foaf: <http://xmlns.com/foaf/0.1/> .\n')
ttlfile.write('@prefix recipe: <http://linkedrecipes.org/schema/> .\n')


# ind = 1
# for tag in tagsFinal:
	# ttlfile.write('cym:tag'+str(ind)+' rdf:type cym:tag .\n')
	# ttlfile.write('cym:tag'+str(ind)+' rdfs:label '+ '"'+tag+'"^^xsd:string .\n')
	# ind += 1
ind = 1
for product in products:
	ttlfile.write('cym:ahproduct'+str(ind)+'\n')
	ttlfile.write('\trdf:type recipe:Food ;\n')
	ttlfile.write('\trdfs:label '+ '"'+product.english_name+'"^^xsd:string ;\n')
	ttlfile.write('\tfoaf:depiction '+ '"'+product.image+'"^^xsd:string ;\n')
	ttlfile.write('\tcym:ahid '+ '"'+product.id+'"^^xsd:string ;\n')
	for tagind in range(len(tagsFinal)):
		if (product.reduced_english_name == tagsFinal[tagind]) or (' '+tagsFinal[tagind]+' ' in product.reduced_english_name) or (product.reduced_english_name.startswith(tagsFinal[tagind]+' ')) or (product.reduced_english_name.endswith(' '+tagsFinal[tagind])):
			ttlfile.write('\tcym:hasTag "'+tagsFinal[tagind].lower()+'" ;\n')
	amount = product.amount.split()
	ttlfile.write('\tcym:availableAt\n')
	ttlfile.write('\t\t[ cym:store "Albert Heijn"^^xsd:string ;\n')
	ttlfile.write('\t\t  cym:price "'+str(product.price)+'"^^xsd:decimal ;\n')
	ttlfile.write('\t\t  cym:amount '+ '"'+amount[0]+'"^^xsd:decimal ;\n')
	ttlfile.write('\t\t  cym:amountMeasure '+ '"'+amount[1]+'"^^xsd:string ] .\n')
	ind += 1
ttlfile.close()



# f = open("all_tagsEN.txt", "w")
# for tag in tagsFinal:
	# f.write(tag+'\t\t:\t\t')
	# for name in names:
		# if (name == tag) or (' '+tag+' ' in name) or (name.startswith(tag+' ')) or (name.endswith(' '+tag)):
			# f.write(name + ',')
	# f.write('\n')
# f.close()

# import goslate
# gs = goslate.Goslate()
# f = open("names.txt", "w")
# id=1
# for product in products:
	# f.write(product.name.encode('utf-8')+"\t\t,\t\t"+gs.translate(product.name, 'en', 'nl').encode('utf-8')+"\n")
	# print id
	# id=id+1
# print len(products)
# f.close()

# from mstranslator import Translator
# translator= Translator('testapp_iwa','dky3x7Iz7rmihw7WZ96Qzpu752vbBVX6JYZwHRR0Yr4=')
# f = open("names.txt", "w")
# id=1
# for product in products:
	# a=translator.translate(product.name, lang_from='nl', lang_to='en')
	# while 'TranslateApiException' in a:
		# a=translator.translate(product.name, lang_from='nl', lang_to='en')
	# f.write(product.name+"\t\t,\t\t"+a+"\n")
	# print id
	# id=id+1
# print len(products)
# f.close()
