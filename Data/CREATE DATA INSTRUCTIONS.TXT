# ------------- ADD FOODISTA DATA --------

# ------------- CREATE INITIAL TAGS --------

PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX recipe: <http://linkedrecipes.org/schema/>
PREFIX cym: <http://cym.com/rdf-schema#>

INSERT { 
    ?x cym:hasTag ?trimmedvalue .
}
WHERE {
	?x rdfs:label ?value .
	?x rdf:type recipe:Food .
	BIND(LCASE(REPLACE(REPLACE(?value, "\t|\n|\r", " "), "^ +| +$", "")) as ?trimmedvalue)
}

# ------------- SPLIT TAGS UNTIL THERE IS NOT ANY CHANGE IN THE DATABASE ANY MORE --------

PREFIX cym: <http://cym.com/rdf-schema#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

DELETE { ?x cym:hasTag ?value }
INSERT { 
    ?x cym:hasTag ?before .
    ?x cym:hasTag ?after .
}
WHERE {
   ?x cym:hasTag ?value .
   FILTER(CONTAINS(?value, " "))
   BIND(REPLACE(strbefore(?value, " "), "^ +| +$", "") as ?before)
   BIND(REPLACE(strafter(?value, " "), "^ +| +$", "") as ?after)
}

# ------------- ADD CYM LABEL TO DISTINGUISM FOODISTA FOODS --------

PREFIX cym: <http://cym.com/rdf-schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX recipe: <http://linkedrecipes.org/schema/>
INSERT { 
    ?x rdf:type cym:foodistaFood .
}
WHERE {
	?x rdf:type recipe:Food .
}

# --------------- CREATE RECIPE DESCRIPTIONS -------------

PREFIX recipe: <http://linkedrecipes.org/schema/>
PREFIX cym: <http://cym.com/rdf-schema#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>

INSERT {
	?recipe cym:recipeDescription ?rec
} WHERE {
	?recipe rdf:type recipe:Recipe .
	?recipe foaf:isPrimaryTopicOf ?desc .
bind (str(?desc) as ?rec)
}

# ------------- CREATE CYM FASHION INGREDIENT STRUCTURE --------

PREFIX cym: <http://cym.com/rdf-schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX recipe: <http://linkedrecipes.org/schema/>

DELETE { ?x recipe:ingredient ?ing }
INSERT {
	?x cym:usesIngredient [ cym:ingredient ?ing ] .
} WHERE {
	?x rdf:type recipe:Recipe .
	?x recipe:ingredient ?ing .
}

# ------------- ADD ALBERT HEIJN DATA NOW --------

# ------------- INSERT PLURAL VERSION OF TAGS --------

PREFIX cym: <http://cym.com/rdf-schema#>

INSERT {
	?x cym:hasTag ?tags .
} WHERE {
	?x cym:hasTag ?tag .
	BIND( fn:concat(?tag, "s") AS ?tags )
}

# ------------- LINK ALBERT HEIJN AND FOODISTA --------

PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX cym: <http://cym.com/rdf-schema#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX recipe: <http://linkedrecipes.org/schema/>

INSERT { 
    ?x cym:availableAt
		[ cym:store ?store;
		  cym:price ?price ;
		  cym:amount ?amount ;
		  cym:amountMeasure ?amountmeasure;
		  cym:productName ?productName] .
}
WHERE {
   ?x cym:hasTag ?value .
   ?x rdf:type cym:foodistaFood .
   ?y cym:hasTag ?value .
   ?y cym:ahid [].
   ?y cym:availableAt ?inst .
   ?inst cym:store ?store ;
		 cym:price ?price ;
		 cym:amount ?amount ;
		 cym:amountMeasure ?amountmeasure ;
		 cym:productName ?productName .
}

# ------------ FINISHED -----------------